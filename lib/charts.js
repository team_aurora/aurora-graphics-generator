const _ = require('lodash');
const moment = require('moment')
const async = require('async')

const formatDate = 'DD-MM-YYYY'

/**
   * Map object for data formats
   * key: the moment unit
   * format: format to compare dates
   * label: label to show date in xAxis
   * @type {Object}
   */
const dateMap = {
  'hour': { key: 'hours', format: 'YYYY-MM-DD HH:mm', label: 'HH:mm' },
  'day': { key: 'days', format: 'YYYY-MM-DD', label: 'D' },
  'week': { key: 'week', format: 'W', label: 'WW' },
  'month': { key: 'months', format: 'YYYY-MM', label: 'MMMM' },
  'year': { key: 'years', format: 'YYYY', label: 'YYYY' },
}


/**
    * Map object for tracing dates
    * @type {Object}
    */
const tracingMap = {
  '0': [moment().hour(0).minute(0), moment().hour(23).minute(59)],
  '1': [moment().subtract(1, 'days').hour(0).minute(0), moment().subtract(1, 'days').hour(23).minute(59)],
  '7': [moment().subtract(7, 'days').hour(0).minute(0), moment().subtract(1, 'days').hour(23).minute(59)],
  '15': [moment().subtract(15, 'days').hour(0).minute(0), moment().subtract(1, 'days').hour(23).minute(59)],
  '30': [moment().subtract(30, 'days').hour(0).minute(0), moment().subtract(1, 'days').hour(23).minute(59)]
}

const keyTranslation = [{
  name: "Load Profile",
  value: "Perfil de Consumo"
},
{
  name: "consumption",
  value: "Lectura de Consumo"
},
{
  name: "temperature",
  value: "Temperatura"
}]

const baseConfig = {
  colors: ['#1F78B4', '#FB7F13', '#2CA12C', '#D62729', '#9366BD', '#E377C3', '#7F7F7F', '#BBBD24', '#2CBECE'],
  title: {
    text: ''
  },
  chart: {
    type: 'column'
  },
  xAxis: {
    categories: [],
  },
  yAxis: {
    labels: {
      formatter: function () {
        return this.value && this.value.toLocaleString(numberFormat);
      }
    }
  },
  credits: {
    enabled: false
  },
  series: [],
  exporting: {
    enabled: false
  },
  tooltip: {
    useHTML: true,
    valueDecimals: 1,
    formatter: function () {
      return `${this.x} <br/>
      ${this.series.name}: <b>${getYAxisFormat(this.y)}</b>`;
    }
  }
}

let isReportWeeklyWithWarning

function calculateAveragePlotLine(series) {
  const serie = series && series.length ? series[0] : null;
  if (serie && serie.data && serie.data.length === 5) {
    const dataLast4Weeks = _.slice(serie.data, 0, serie.data.length - 1)
    const average = _.sum(dataLast4Weeks) / 4;
    const lastSerieData = serie.data[4];
    if (lastSerieData >= average) {
      isReportWeeklyWithWarning = true
    }
    else {
      isReportWeeklyWithWarning = false
    }
    return average;
  }
}

function getYAxisFormat(value, serieName) {
  let options = { maximumFractionDigits: 1, minimumFractionDigits: 1 }

  if (serieName && (serieName.includes('fft') || serieName.includes('vibration'))) {
    options = { maximumFractionDigits: 10, minimumFractionDigits: 1 }
  }
  return value.toLocaleString(numberFormat, options)
}

function getConfigurationYAxis(options) {
  const plotLine = options.averageConsumption ? calculateAveragePlotLine(options.series) : 0
  const metricField = options.metricField;
  const chartData = options.chartData;
  const functionGraph = options.function === 'none' ? undefined : options.function;
  const waterOrGasService = metricField.services.find(e => e.name === 'water' || e.name === 'gas');
  const doorStatusService = metricField.services.find(e => e.name === 'door_status');
  const temperatureService = metricField.services.find(e => e.name === 'temperature');
  const energyService = metricField.services.find(e => e.name === 'energy');
  const doorOpenService = metricField.services.find(e => e.name === 'aperture');
  const waterOrGas = chartData.find(e => e.service === 'water' || e.service === 'gas');
  const energy = chartData.find(e => e.service === 'energy');
  const temperature = chartData.find(e => e.service === 'temperature');
  const iee = chartData.find(e => e.service === 'energy_efficiency');
  const udcServices = metricField.services.find(e => e.name === 'udc');
  const udc = chartData.find(e => e.service === 'udc');
  const labels = energyService ? [`main.views.${energyService.label}`] : [];
  if (waterOrGasService) {
    labels.push(`main.views.${waterOrGasService.label}`);
  }
  if (temperatureService) {
    labels.push(`main.views.${temperatureService.label}`);
  }
  if (doorOpenService) {
    labels.push(`main.views.${doorOpenService.label}`);
  }
  if (doorStatusService) {
    labels.push(`main.views.${doorStatusService.label}`);
  }
  const results = [];

  if (energy) {
    const titleEnergy = energyService && _.find(keyTranslation, { 'name': energyService.label }).value;
    const opposite = options.opposite ? options.opposite : (results.length >= 1 ? true : false);
    if (options.averageConsumption && plotLine) {
      const plotLines = [];
      plotLines.push({ color: 'red', value: plotLine, width: 2 });
      results.push({ min: 0, labels: { format: '{value}' }, title: { text: `${titleEnergy} ${getUnitFormatForAxis(energyService.unit)}` }, opposite, labels: baseConfig.yAxis.labels, type: functionGraph, plotLines: plotLines });
    }
    else {
      results.push({ min: 0, labels: { format: '{value}' }, title: { text: `${titleEnergy} ${getUnitFormatForAxis(energyService.unit)}` }, opposite, labels: baseConfig.yAxis.labels, type: functionGraph });
    }
  }
  if (waterOrGas) {
    const titlewaterOrGas = waterOrGasService && _.find(keyTranslation, { 'name': waterOrGasService.label }).value;
    const opposite = options.opposite ? options.opposite : (results.length >= 1 ? true : false);
    results.push({ min: 0, labels: { format: '{value}' }, title: { text: `${titlewaterOrGas} ${getUnitFormatForAxis(waterOrGasService.unit)}` }, opposite, labels: baseConfig.yAxis.labels, type: functionGraph });
  }
  if (temperature && !doorOpenService) {
    if (metricField.key === 'temperature') {
      const titleTemperature = temperatureService && _.find(keyTranslation, { 'name': temperatureService.label }).value;
      results.push({ labels: { format: '{value}' }, title: { text: `${titleTemperature} ${getUnitFormatForAxis(temperatureService.unit)}` }, opposite: 0, labels: baseConfig.yAxis.labels, type: functionGraph });
    }
    if (metricField.key === 'door_status') {
      const titleDoor = _.find(keyTranslation, { 'name': doorStatusService.label }).value;
      const opposite = options.opposite ? options.opposite : (results.length >= 1 ? true : false);
      results.push({
        labels: { format: '{value}' }, title: { text: titleDoor }, opposite, labels: baseConfig.yAxis.labels,
        tickPositioner: function () {
          return [this.dataMin, this.dataMax];
        }
      });
    }
  }
  if (doorOpenService) {
    const titleDoorOpen = doorOpenService.label && _.find(keyTranslation, { 'name': doorOpenService.label }).value;
    results.push({ labels: { format: '{value}' }, title: { text: titleDoorOpen }, opposite: 0, labels: baseConfig.yAxis.labels, type: functionGraph });
    results.push({ labels: { format: '{value}%' }, title: { text: `${titleDoorOpen} %` }, opposite: 1, labels: baseConfig.yAxis.labels });
  }

  return results;
}


function getUnitFormatForAxis(unit) {
  if (!unit) {
    return '';
  }
  return `(${unit})`;
}

function getYAxisFormat(value) {
  const options = { maximumFractionDigits: 1, minimumFractionDigits: 1 };
  return value.toLocaleString(numberFormat, options);
}

function buildDateXAxis(pattern, granularity, fields) {
  var xAxis = []

  pattern = _.uniq(pattern)
  granularity = granularity || 'hour'
  const isFFT = _.some(fields, (f) => f.metric === 'fft')

  if (!pattern && (granularity === 'hour' || granularity === 'day') && !isFFT) {
    return undefined
  }
  for (var i = 0; i < pattern.length; i++) {
    if (granularity === 'month') {
      xAxis.push(moment(pattern[i]).format(dateMap[granularity].label) + '-' + pattern[i].split('-')[0])
    } else {
      if (!moment(pattern[i]).isValid() || granularity === 'week' || granularity === 'year' || isFFT) {
        xAxis.push(pattern[i])
      } else {
        xAxis.push(moment(pattern[i]).format(dateMap[granularity].label))
      }
    }
  }
  return xAxis
}

function getConfigurationXAxis(options) {
  const type = 'datetime'
  let pattern

  if (options.pattern) {
    pattern = _.reduce(options.pattern, (results, pattern) => {
      _.forEach(options.chartData, (chart) => {
        if (_.find(chart.data, { dateString: pattern })) {
          results.push(pattern)
          return results
        }
      })
      return results
    }, [])

    if (pattern.length === 0) {
      pattern = _.reduce(options.chartData, (results, chart) => {
        _.forEach(chart.data, (data) => {
          const dateString = data.dateString

          if (results.indexOf(dateString) === -1) {
            results.push(dateString)
          }
        })
        return results
      }, [])
    }
  }
  const result = {
    startOnTick: true,
    endOnTick: false,
    categories: buildDateXAxis(pattern, options.granularity, options.fields),
    title: {
      text: moment(options.dates[0]).format(formatDate) + ' - ' + moment(options.dates[1]).format(formatDate)
    },
    type,
    crosshair: {
      width: 1,
      color: '#cccccc'
    }
  }

  return result
}

function getConfigurationSeries(serieName, fieldName, element, granularity, services, options) {
  let data = element.data
  let x
  let dateTooltip
  // const service = element.service
  // let stacking
  let frequency

  if (options.elementsObj && options.elementsObj.length > 0) {
    const elementObj = _.find(options.elementsObj, (elementObj) => elementObj.elementId === element.id)

    if (elementObj) {
      const code = _.find(elementObj.schemaData.firmware.codes, (code) => {
        return _.every(options.fields, (field) => {
          return _.map(code.settings, (e) => e.fieldName).indexOf(field === 'consumptionProfile' ? 'consumption' : field) > -1
        })
      })

      frequency = code && code.frequency
    }
  }
  if (services && (services[0] === 'vibration' || services[0] === 'rpm' || services[0] === 'flow' || services[0] === 'revolution')) {
    frequency = 60
  }
  const serie = {
    yAxis: options.yAxis,
    pointPadding: options.pointPadding || 0.1,
    lineWidth: options.lineWidth !== undefined ? options.lineWidth : 2,
    fillOpacity: options.fillOpacity || 0.30,
    dataLabels: options.dataLabels || false, // it does not work
    enableMouseTracking: options.enableMouseTracking || true, // it does not work
    name: serieName,
    turboThreshold: data.length,
    type: options.type,
    metric: options.metric,
    zIndex: options.zIndex || (options.metric === 'load_profile' && (100 || undefined)),
    // stacking: 'normal',
    data: _.reduce(data, function (obj, value, index) {
      if (value && value[fieldName] !== undefined) {
        const y = parseFloat(value[fieldName])

        if (value.position && (granularity === 'month' || granularity === 'week' || granularity === 'year')) {
          obj[value.position] = y
        } else {
          if (isNaN(value.dateString) && moment(value.dateString).isValid() && (!granularity || granularity === 'hour' || granularity === 'day')) {
            x = moment(value.dateString, 'YYYY-MM-DD HH:mm:ss')
            dateTooltip = x.clone().valueOf()
            if (options.metric === 'consumption' && granularity === 'day' && y > 0) {
              obj.push({ y: y, x: x.valueOf(), dateTooltip: dateTooltip })
            }
            // else if (y != 0) {
            if (granularity === 'hour' && frequency && options.metric === 'load_profile') {
              x = x.clone().subtract(frequency / 2, 'minute')
            }
            obj.push({ y: y, x: x.valueOf(), max: value.max, min: value.min, dateTooltip: dateTooltip })
            // }
          } else if (!isNaN(value.dateString) && granularity === 'hour' && options.metric !== 'fft') {
            obj.push({ y: y, x: parseInt(value.dateString) })
          } else {
            obj.push(y)
          }
        }
      } else {
        if (granularity === 'hour' && frequency && value[fieldName] === null) {
          x = moment(value.dateString, 'YYYY-MM-DD HH:mm:ss')
          dateTooltip = x.clone().valueOf()
          if (options.metric === 'load_profile') {
            x = x.clone().subtract(frequency / 2, 'minute')
          }
          obj.push({ y: null, x: x.valueOf(), max: value.max, min: value.min, dateTooltip: dateTooltip })
        }
      }
      return obj
    }, []),
    marker: {
      enabled: data.length <= 25,
      radius: 3
    }
  }

  return serie
}

function getRangeDates(startDate, stopDate, granularity, fields, chartData) {
  var dateArray = []
  const openDoorFields = (fields && chartData) && _.find(fields, (field) => field.metric === 'door_opening')

  if (openDoorFields) {
    _.find(chartData, (element) => {
      _.forEach(element.data, (data) => {
        if (_.isEmpty(data[openDoorFields.field])) {
          dateArray.push(data.dateString)
        }
      })
      return dateArray.length > 0
    })
    return dateArray
  }
  const fftField = _.find(fields, (f) => f.metric === 'fft')

  if (fftField) {
    _.forEach(chartData, (element) => {
      _.forEach(element.data, (data) => {
        if (_.isEmpty(data[fftField.fields[0]])) {
          dateArray.push(data.dateString)
        }
      })
    })
    return dateArray
  }

  if (!granularity || granularity === 'hour' || granularity === 'day') {
    return undefined
  }
  var currentDate = moment(startDate).hour(0).minute(0)

  stopDate = moment(stopDate).hour(23).minute(59)

  while (granularity === 'year' ? moment(currentDate).format(dateMap[granularity].format) <= moment(stopDate).format(dateMap[granularity].format)
    : granularity === 'week' ? currentDate <= stopDate || moment(currentDate).format(dateMap[granularity].format) === moment(stopDate).format(dateMap[granularity].format)
      : granularity === 'month' ? currentDate <= stopDate || dateArray.indexOf(stopDate.format(dateMap[granularity].format)) < 0 : currentDate <= stopDate) {
    dateArray.push(moment(currentDate).format(dateMap[granularity].format))
    currentDate = moment(currentDate).add(1, dateMap[granularity].key)
  }
  return dateArray
}

function defineOpposite(fields, metricField) {
  let result = false
  if (fields.length > 1) {
    if (metricField.key === 'load_profile'
      || metricField.key === 'door_status'
      || metricField.key === 'reactive_consumption'
      || metricField.key === 'reactive_load_profile') {
      result = true
    }
    if (fields.find(f => f.metric === 'reactive_load_profile') && metricField.key === 'reactive_consumption') {
      result = false
    } else if (fields.find(f => f.metric === 'reactive_consumption') && metricField.key === 'load_profile') {
      result = false
    } else if (fields.find(f => f.metric === 'reactive_load_profile') && metricField.key === 'load_profile') {
      result = false
    }
  }
  return result
}

function orderSeriesByPosition(series, categories, granularity) {
  _.each(series, (serie) => {
    if (granularity === 'month') {
      serie.position = _.findIndex(categories, (category) => {
        const split = category.split('-')
        const date = split[1] + '-' + moment().month(split[0]).format("MM")
        return date == serie.dateString
      })
    }
    else {
      serie.position = _.findIndex(categories, (category) => {
        return category === serie.dateString
      })
    }
  })
  return series;
}

function getSeriesName(fields, field, element, fieldsTranslate) {
  if (fields.length > 1) {
    return element.name + ' - ' // fieldsTranslate['main.views.' + field]
  } else if (fields[0].metric !== 'vibration') {
    return element.name
  }
  return fieldsTranslate['main.views.' + field]
}

function singleChart(chartSetting, chartData, fields, fieldsTranslate, metricObject) {
  var isEmpty = true
  var dates = (chartSetting.tracing) ? tracingMap[chartSetting.tracingDays || 0] : chartSetting.date.firstDate
  var config = _.cloneDeep(baseConfig)
  var pattern = getRangeDates(dates[0], dates[1], chartSetting.granularity, fields, chartData) // improve is called
  var noData = []
  var services = _.uniq(_.map(chartData, function (element) { return element.service }))
  var chartDataLength = chartData.length

  config.xAxis = getConfigurationXAxis({ pattern, granularity: chartSetting.granularity, dates, chartData, fields })
  _.forEach(chartData, function (element) {
    _.maxBy(element.data, 'dateString')

    if (chartSetting.granularity === 'hour' && element.data && element.data.length === 0) {
      // if (element.data.length > 0) {
      //   var min = _.minBy(element.data, 'dateString')
      //   var max = _.maxBy(element.data, 'dateString')
      // pattern = getRangeDates(min.dateString, max.dateString, chartSetting.granularity)
      // } else {
      noData.push(element)
      // }
    }
    _.forEach(fields, function (field, i) {
      let serieName
      let serie
      let options = { metric: field.metric, elementsObj: chartSetting.elements, fields: field.fields }

      if (field.fields.length > 0) {
        field.fields.forEach((f) => {
          serieName = getSeriesName(fields, f, element, fieldsTranslate)
          if (f === 'doorStatus') {
            options = chartSetting.granularity === 'hour' ? { type: 'area', lineWidth: 0.1, fillOpacity: 0.3 } : {
              type: 'line',
              dataLabels: {
                enabled: true
              },
              enableMouseTracking: false
            }
          } else if (f === 'iee') {
            options = {
              type: 'column'
            }
          } else if (f === 'litersMinute') {
            options = {
              type: 'line'
            }
          }
          if (chartSetting.granularity === 'month' || chartSetting.granularity === 'week' || chartSetting.granularity === 'year') {
            serie = orderSeriesByPosition(element.data, config.xAxis.categories, chartSetting.granularity)
          }
          serie = getConfigurationSeries(serieName, f, element, chartSetting.granularity, services, options)
          if (f === 'openDoorNumber') {
            const options = {
              yAxis: 1,
              type: 'column',
              dataLabels: {
                enabled: true
              },
              enableMouseTracking: false
            }
            // serie.type = 'spline'
            // serie.zIndex= 10

            config.series.push(getConfigurationSeries(serieName, 'percentageOpenDoorTime', element, chartSetting.granularity, services, options))
          } else if (f === 'iee') {
            const options = {
              // yAxis: 1,
              type: 'line',
              zIndex: 100
            }

            config.series.push(getConfigurationSeries(`${serieName} (${fieldsTranslate[`main.views.goal`]})`, 'goal', element, chartSetting.granularity, services, options))
          }
          isEmpty = (isEmpty && serie.data && serie.data.length > 0) && false
          config.series.push(serie)
        })
      }
    })
    _.each(config.series, (s) => {
      const arrayNull = s.data.filter((d) => !d)

      if (arrayNull.length === s.data.length) {
        s.data = []
      }
      if (!chartSetting.setType && s.data.length === 1) {
        chartSetting.type = 'column'
      }
    })
  })
  config.chart.type = chartSetting.type || 'line'
  // verify complete week series

  if (chartSetting.granularity === 'week' || chartSetting.granularity === 'month' || chartSetting.granularity === 'year') {
    _.map(config.series, (serie) => {
      if (config.xAxis.categories.length < serie.data.length - 1) {
        if (chartSetting.granularity === 'week') {
          serie.data = _.filter(serie.data, (data, key) => {
            if (_.includes(config.xAxis.categories, key.toString())) {
              if (!data) return ''
              else return data
            }
          })
        } else {
          serie.data = _.filter(serie.data, (data, key) => {
            let flagMonthInclude = false

            _.map(config.xAxis.categories, (category) => {
              if (moment().month(category).month() + 1 === key) {
                flagMonthInclude = true
              }
            })
            if (flagMonthInclude) {
              if (!data) return ''
              else return data
            }
          })
        }
      } else {
        serie.data = _.map(serie.data, (data) => {
          if (!data) return ''
          return data
        })
      }
    })
  }
  config.series = _.map(config.series, (serie) => {
    serie.turboThreshold = serie.data.length
    return serie
  })
  const minDate = dates[0] instanceof moment ? _.cloneDeep(dates[0]).set({ h: 0, m: 0, s: 0, ms: 0 }) : moment(`${dates[0]} 00:00`)
  const maxDate = dates[1] instanceof moment ? _.cloneDeep(dates[1]).set({ h: 23, m: 59, s: 59 }) : moment(`${dates[1]} 23:59`)

  if (config.series.length < 3 && chartSetting.granularity === 'hour' && moment.duration(maxDate.diff(minDate)).asHours() <= 24) {
    _.each(config.series, (s) => {
      if (s.data < 7) {
        s.pointWidth = 10
      }
    })
  }

  if (chartSetting.granularity === 'hour') {
    config.xAxis.startOnTick = false
    config.xAxis.endOnTick = _.find(fields, (field) => field.metric === 'load_profile') && true // to leave on 00:00:00 after 23:00:00
    config.xAxis.labels = {
      formatter: function () {
        const label = moment(this.value)

        if (validDate(label)) {
          return label.format('HH:mm')
        }
        return this.value
      }
    }
    if (!services.find((e) => e === 'temperature' || e === 'vibration')) {
      config.xAxis.tickInterval = 1 * 3600 * 1000
      config.xAxis.min = minDate.valueOf()
      const dataConsumption = chartData.find((element) => element.data.length > 24)

      if (!dataConsumption) {
        config.xAxis.max = maxDate.valueOf()
      }
      // config.chart = {
      //   zoomType: 'x'
      // }
    }
    const formatDate = maxDate.diff(minDate, 'day') > 1 ? 'DD-MM-YYYY HH:mm' : 'HH:mm:ss.SSSS'

    config.tooltip = {
      useHTML: true,
      formatter: function () {
        const date = moment(this.x)
        let tooltip = `${validDate(date) ? date.format(formatDate) : this.x} <br/>
        ${this.series.name}: <b>${getYAxisFormat(this.y, this.series.name)}</b>`
        const serie = _.find(this.series.data, (data) => {
          return data.x === this.x && data.y === this.y
        })

        if (serie && serie.dateTooltip) {
          tooltip = `${moment(serie.dateTooltip).format(formatDate)} <br/>
          ${this.series.name}: <b>${getYAxisFormat(this.y, this.series.name)}</b>`
        }
        return tooltip
      }
    }
  } else if (!chartSetting.granularity || chartSetting.granularity === 'day') {
    config.xAxis.startOnTick = false
    config.xAxis.endOnTick = false
    config.xAxis.labels = {
      formatter: function () {
        return moment(this.value).format('DD')
      }
    }
    // Check if the information is in series if it is so eliminates the categories.
    // This to avoid repeating the information on the Xaxis
    if (config.series && config.series.length && config.xAxis.categories) {
      delete config.xAxis.categories
    }
    config.tooltip = {
      useHTML: true,
      valueDecimals: 1,
      formatter: function () {
        let tooltip = `${moment(this.x).format(formatDate)} <br/>
        ${this.series.name}: <b>${getYAxisFormat(this.y)}</b>`
        const serie = _.find(this.series.data, (data) => {
          return data.x === this.x && data.y === this.y
        })

        if (serie && serie.min && serie.max) {
          tooltip += `<br/>
          min: <b>${getYAxisFormat(serie.min)}</b> <br/>
          max: <b>${getYAxisFormat(serie.max)}</b>`
        }
        return tooltip
      }
    }
  }
  config.yAxis = []
  config.yAxis.min = 0

  const yAxisPromises = [];
  _.map(fields, function (field, i) {
    var metricField = _.find(metricObject, function (a) {
      return a.key == field.metric
    });
    metricField.key = metricField && metricField.key ? metricField.key : 'consumption';
    if (config.series.length > 1) {
      _.forEach(config.series, serie => {
        if (!serie.yAxis) {
          if (!serie.metric) {
            serie.yAxis = 0;
          } else if (serie.metric === metricField.key) {
            serie.yAxis = i;
          }
        }
      });
    }
    const opposite = defineOpposite(fields, metricField)
    yAxisPromises.push(getConfigurationYAxis({ series: config.series, averageConsumption: chartSetting.averageConsumption, metricField, chartData, function: chartSetting.function, opposite: (fields.length > 1 && (metricField.key === 'load_profile' || metricField.key === 'door_status')) }));
  });
  if (chartSetting.averageConsumption) {
    config.series.push({
      name: "Consumo promedio",
      color: 'red',
      dashStyle: 'solid',
      type: 'line',
      marker: {
        enabled: false
      }
    })
    config.reportWeeklyWarning = isReportWeeklyWithWarning
  }

  config.isEmpty = isEmpty
  if (noData.length == chartDataLength) {
    $translate('main.views.noData').then(function (translation) {
      toastr.info(translation)
    });
  }
  async.forEachOf(yAxisPromises, function (value, key, callback) {
    config.yAxis = config.yAxis.concat(value);
  }, function (err) {
    if (err) return ("ERROR ", err);
  });
  return config;
}

module.exports = {
  singleChart
}
